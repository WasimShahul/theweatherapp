package com.wasim.hungrr.theweatherapp.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class ForecastMainModel {

    private String temp;
    private String temp_min;
    private String temp_max;
    private String pressure;
    private String sea_level;
    private String grnd_level;
    private String humidity;
    private String temp_kf;

    public ForecastMainModel() {
    }

    public ForecastMainModel(String temp, String temp_min, String temp_max, String pressure, String sea_level, String grnd_level, String humidity, String temp_kf) {
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.pressure = pressure;
        this.sea_level = sea_level;
        this.grnd_level = grnd_level;
        this.humidity = humidity;
        this.temp_kf = temp_kf;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTemp_max() {
        return temp_max;
    }

    public void setTemp_max(String temp_max) {
        this.temp_max = temp_max;
    }

    public String getTemp_min() {
        return temp_min;
    }

    public void setTemp_min(String temp_min) {
        this.temp_min = temp_min;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public void setSea_level(String sea_level) {
        this.sea_level = sea_level;
    }

    public String getSea_level() {
        return sea_level;
    }

    public void setGrnd_level(String grnd_level) {
        this.grnd_level = grnd_level;
    }

    public String getGrnd_level() {
        return grnd_level;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setTemp_kf(String temp_kf) {
        this.temp_kf = temp_kf;
    }

    public String getTemp_kf() {
        return temp_kf;
    }
}
