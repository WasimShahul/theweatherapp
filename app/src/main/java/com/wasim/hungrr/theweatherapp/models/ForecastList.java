package com.wasim.hungrr.theweatherapp.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class ForecastList {
    private String dt_txt;
    private String dt;
    ForecastMainModel forecastMainModel;
    ForecastWeather forecastWeather;
    ForecastWind forecastWind;
    ForecastClouds forecastClouds;

    public ForecastList() {
    }

    public ForecastList(String dt_txt, String dt, ForecastMainModel forecastMainModel, ForecastWeather forecastWeather, ForecastWind forecastWind, ForecastClouds forecastClouds) {
        this.dt_txt = dt_txt;
        this.dt = dt;
        this.forecastMainModel = forecastMainModel;
        this.forecastWeather = forecastWeather;
        this.forecastWind = forecastWind;
        this.forecastClouds = forecastClouds;
    }

    public ForecastClouds getForecastClouds() {
        return forecastClouds;
    }

    public ForecastMainModel getForecastMainModel() {
        return forecastMainModel;
    }

    public ForecastWeather getForecastWeather() {
        return forecastWeather;
    }

    public ForecastWind getForecastWind() {
        return forecastWind;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public void setForecastClouds(ForecastClouds forecastClouds) {
        this.forecastClouds = forecastClouds;
    }

    public void setForecastMainModel(ForecastMainModel forecastMainModel) {
        this.forecastMainModel = forecastMainModel;
    }

    public void setForecastWeather(ForecastWeather forecastWeather) {
        this.forecastWeather = forecastWeather;
    }

    public void setForecastWind(ForecastWind forecastWind) {
        this.forecastWind = forecastWind;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getDt() {
        return dt;
    }
}
