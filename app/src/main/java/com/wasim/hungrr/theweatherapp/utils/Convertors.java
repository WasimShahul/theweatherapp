package com.wasim.hungrr.theweatherapp.utils;

import java.util.Date;

/**
 * Created by Wasim on 5/26/2018.
 */

public class Convertors {

    public static int kelvinToCelcius(Float kelvin) {
        return (int) (kelvin - 273.15F);
    }

    public static double kelvinToFarenheit(Float kelvin) {
        Float celsius = Float.valueOf(kelvinToCelcius(kelvin));

        return (celsius * 9.0/5.0) + 32.0;
    }

    public static Date convertToDate(String unixTimeStamp)
    {
        return new Date(Long.valueOf(unixTimeStamp) * 1000);
    }

}
