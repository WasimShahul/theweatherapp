package com.wasim.hungrr.theweatherapp.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class ForecastWind {
    private String speed;
    private String deg;

    public ForecastWind() {
    }

    public ForecastWind(String speed, String deg) {
        this.speed = speed;
        this.deg = deg;
    }

    public String getDeg() {
        return deg;
    }

    public String getSpeed() {
        return speed;
    }

    public void setDeg(String deg) {
        this.deg = deg;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
