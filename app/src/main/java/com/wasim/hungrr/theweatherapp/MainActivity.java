package com.wasim.hungrr.theweatherapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nabinbhandari.android.permissions.PermissionHandler;
import com.nabinbhandari.android.permissions.Permissions;
import com.wasim.hungrr.theweatherapp.models.ForecastClouds;
import com.wasim.hungrr.theweatherapp.models.ForecastList;
import com.wasim.hungrr.theweatherapp.models.ForecastMainModel;
import com.wasim.hungrr.theweatherapp.models.ForecastWeather;
import com.wasim.hungrr.theweatherapp.models.ForecastWind;
import com.wasim.hungrr.theweatherapp.utils.Constant;
import com.wasim.hungrr.theweatherapp.utils.Convertors;
import com.wasim.hungrr.theweatherapp.utils.GPSTracker;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MainActivity";
    private ArrayList<ForecastList> dailyForeCastList = new ArrayList<ForecastList>();

    TextView locationTV, weatherTV, degreeTV, timeTV;
    TextView windTV, humidityTV, date1, date2, date3, weather1, weather2, weather3;
    RadioRealButtonGroup buttonGroup;
    RadioRealButton celciusButton, farenheitButton;

    GPSTracker gps;
    Double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationTV = (TextView) findViewById(R.id.locationTV);
        weatherTV = (TextView) findViewById(R.id.weatherTV);
        degreeTV = (TextView) findViewById(R.id.degreeTV);
        timeTV = (TextView) findViewById(R.id.timeTV);
        windTV = (TextView) findViewById(R.id.windTV);
        humidityTV = (TextView) findViewById(R.id.humidityTV);
        date1 = (TextView) findViewById(R.id.date1);
        date2 = (TextView) findViewById(R.id.date2);
        date3 = (TextView) findViewById(R.id.date3);
        weather1 = (TextView) findViewById(R.id.weather1);
        weather2 = (TextView) findViewById(R.id.weather2);
        weather3 = (TextView) findViewById(R.id.weather3);
        buttonGroup = (RadioRealButtonGroup) findViewById(R.id.buttonGroup);
        celciusButton = (RadioRealButton) findViewById(R.id.celciusButton);
        farenheitButton = (RadioRealButton) findViewById(R.id.farenheitButton);

        //Implement Onclick listeners
        locationTV.setOnClickListener(this);
        weatherTV.setOnClickListener(this);
        degreeTV.setOnClickListener(this);
        timeTV.setOnClickListener(this);
        celciusButton.setSelected(true);

        gps = new GPSTracker(MainActivity.this);

        buttonGroup.setOnClickedButtonListener(new RadioRealButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(RadioRealButton button, int position) {
                getForecast();
                getCurrentWeather();
            }
        });

        Permissions.check(this, Manifest.permission.ACCESS_FINE_LOCATION, null,
                new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        if (gps.canGetLocation()) {
                            latitude = gps.getLatitude();
                            longitude = gps.getLongitude();
                            RequestParams params = new RequestParams();
                            params.add("appid", Constant.APPID);
                            params.add("lat", String.valueOf(gps.getLatitude()));
                            params.add("lon", String.valueOf(gps.getLongitude()));
                            getForecast(params);
                            getCurrentWeather(params);
                        } else {
                            RequestParams params = new RequestParams();
                            params.add("appid", Constant.APPID);
                            params.add("q", "Chennai,IN");
                            getForecast(params);
                            getCurrentWeather(params);
                        }
                    }
                });

    }

    private void getCurrentWeather(RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get(Constant.CURRECT_WEATHER, params, new GetCurrentWeatherResponsehandler());
    }

    private void getForecast(RequestParams params) {
        AsyncHttpClient client = new AsyncHttpClient();
        final int DEFAULT_TIMEOUT = 20 * 10000;
        client.setTimeout(DEFAULT_TIMEOUT);
        client.get(Constant.FORECAST, params, new GetForecastResponsehandler());
    }

    private void setCurrentWeather(ForecastList forecastList) {
        weatherTV.setText(forecastList.getForecastWeather().getMain());
        if(celciusButton.isChecked()){
            degreeTV.setText(Convertors.kelvinToCelcius(Float.valueOf(forecastList.getForecastMainModel().getTemp()))+"°");
        } else {
            degreeTV.setText(Convertors.kelvinToFarenheit(Float.valueOf(forecastList.getForecastMainModel().getTemp()))+"°");
        }
        timeTV.setText(Convertors.convertToDate(forecastList.getDt()) + "");
        windTV.setText(forecastList.getForecastWind().getSpeed()+" km/h");
        humidityTV.setText(forecastList.getForecastMainModel().getHumidity()+"%");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.locationTV:
                break;

            case R.id.weatherTV:
                break;

            case R.id.degreeTV:
                break;

            case R.id.timeTV:
                break;

            default:
                break;
        }
    }

    public class GetCurrentWeatherResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

            try {
                JsonObject rootJsonObject = new Gson().fromJson(new String(responseBody), JsonObject.class);
                JsonPrimitive dt_obj = rootJsonObject.getAsJsonPrimitive("dt");
                String dt = dt_obj.getAsString();
                JsonObject mainObject = rootJsonObject.getAsJsonObject("main");
                JsonObject windObject = rootJsonObject.getAsJsonObject("wind");
                JsonObject cloudsObject = rootJsonObject.getAsJsonObject("clouds");
                JsonElement weatherArray = rootJsonObject.getAsJsonArray("weather").get(0);
                ForecastMainModel forecastMainModel = new Gson().fromJson(String.valueOf(mainObject), ForecastMainModel.class);
                ForecastWind forecastWind = new Gson().fromJson(String.valueOf(windObject), ForecastWind.class);
                ForecastClouds forecastClouds = new Gson().fromJson(String.valueOf(cloudsObject), ForecastClouds.class);
                ForecastWeather forecastWeather = new Gson().fromJson(String.valueOf(weatherArray), ForecastWeather.class);
                ForecastList forecastList = new ForecastList("", dt, forecastMainModel, forecastWeather, forecastWind, forecastClouds);
                setCurrentWeather(forecastList);
                Log.e(TAG, Convertors.convertToDate(dt) + "");
            } catch (Exception e) {

            }

        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(MainActivity.this, "Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }


    public class GetForecastResponsehandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
            try {
                JsonObject rootJsonObject = new Gson().fromJson(new String(responseBody), JsonObject.class);
                JsonArray root = rootJsonObject.getAsJsonArray("list");
                for (int i = 0; i < root.size(); i++) {
                    JsonElement listElement = root.get(i);
                    JsonObject listObject = listElement.getAsJsonObject();
                    JsonElement weatherArray = listObject.getAsJsonArray("weather").get(0);
                    JsonPrimitive dt_txt_obj = listObject.getAsJsonPrimitive("dt_txt");
                    String dt_txt = dt_txt_obj.getAsString();
                    JsonPrimitive dt_obj = listObject.getAsJsonPrimitive("dt");
                    String dt = dt_obj.getAsString();
                    ForecastMainModel forecastMainModel = new Gson().fromJson(String.valueOf(listObject.getAsJsonObject("main")), ForecastMainModel.class);
                    ForecastWind forecastWind = new Gson().fromJson(String.valueOf(listObject.getAsJsonObject("wind")), ForecastWind.class);
                    ForecastClouds forecastClouds = new Gson().fromJson(String.valueOf(listObject.getAsJsonObject("clouds")), ForecastClouds.class);
                    ForecastWeather forecastWeather = new Gson().fromJson(String.valueOf(weatherArray), ForecastWeather.class);
                    ForecastList forecastList = new ForecastList(dt_txt, dt, forecastMainModel, forecastWeather, forecastWind, forecastClouds);
                    if(dt_txt.contains("00:00:00")){
                        dailyForeCastList.add(forecastList);
                    }
                }
                setDailyForecast(dailyForeCastList);
            } catch (Exception e) {

            }
        }

        @Override
        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            Toast.makeText(MainActivity.this, "Seems like your network connectivity is down or very slow", Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("SetTextI18n")
    private void setDailyForecast(ArrayList<ForecastList> dailyForeCastList) {
        date1.setText(dailyForeCastList.get(0).getDt_txt());
        date2.setText(dailyForeCastList.get(1).getDt_txt());
        date3.setText(dailyForeCastList.get(2).getDt_txt());
        if(celciusButton.isChecked()){
            weather1.setText(Convertors.kelvinToCelcius(Float.valueOf(dailyForeCastList.get(0).getForecastMainModel().getTemp_min()))+"° / "+Convertors.kelvinToCelcius(Float.valueOf(dailyForeCastList.get(0).getForecastMainModel().getTemp_max()))+"°");
            weather2.setText(Convertors.kelvinToCelcius(Float.valueOf(dailyForeCastList.get(1).getForecastMainModel().getTemp_min()))+"° / "+Convertors.kelvinToCelcius(Float.valueOf(dailyForeCastList.get(1).getForecastMainModel().getTemp_max()))+"°");
            weather3.setText(Convertors.kelvinToCelcius(Float.valueOf(dailyForeCastList.get(2).getForecastMainModel().getTemp_min()))+"° / "+Convertors.kelvinToCelcius(Float.valueOf(dailyForeCastList.get(2).getForecastMainModel().getTemp_max()))+"°");
        } else {
            weather1.setText(Convertors.kelvinToFarenheit(Float.valueOf(dailyForeCastList.get(0).getForecastMainModel().getTemp_min()))+"° / "+Convertors.kelvinToFarenheit(Float.valueOf(dailyForeCastList.get(0).getForecastMainModel().getTemp_max()))+"°");
            weather2.setText(Convertors.kelvinToFarenheit(Float.valueOf(dailyForeCastList.get(1).getForecastMainModel().getTemp_min()))+"° / "+Convertors.kelvinToFarenheit(Float.valueOf(dailyForeCastList.get(1).getForecastMainModel().getTemp_max()))+"°");
            weather3.setText(Convertors.kelvinToFarenheit(Float.valueOf(dailyForeCastList.get(2).getForecastMainModel().getTemp_min()))+"° / "+Convertors.kelvinToFarenheit(Float.valueOf(dailyForeCastList.get(2).getForecastMainModel().getTemp_max()))+"°");

        }
    }

}
