package com.wasim.hungrr.theweatherapp.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class ForecastWeather {
    private String id;
    private String main;
    private String description;
    private String icon;

    public ForecastWeather() {
    }

    public ForecastWeather(String id, String main, String description, String icon) {
        this.id = id;
        this.main = main;
        this.description = description;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public String getMain() {
        return main;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMain(String main) {
        this.main = main;
    }

}
