package com.wasim.hungrr.theweatherapp.models;

/**
 * Created by Wasim on 5/14/2018.
 */

public class ForecastClouds {
    private String all;

    public ForecastClouds() {
    }

    public ForecastClouds(String all) {
        this.all = all;
    }

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

}
