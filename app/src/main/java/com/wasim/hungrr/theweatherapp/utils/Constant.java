package com.wasim.hungrr.theweatherapp.utils;

/**
 * Created by Wasim on 5/26/2018.
 */

public class Constant {
    public static final String BASE_URL = "http://api.openweathermap.org/";
    public static final String FORECAST = BASE_URL + "data/2.5/forecast";
    public static final String CURRECT_WEATHER = BASE_URL + "data/2.5/weather";
    public static final String APPID = "91583668ea4e1430f3a9a40043955470";
}
